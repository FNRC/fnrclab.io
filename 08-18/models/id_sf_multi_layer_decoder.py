"""Slot Tagger models."""
import torch
import torch.nn as nn
import torch.nn.functional as F

import models.crf as crf
from models.self_attend import SelfAttend

from pytorch_transformers.modeling_utils import SequenceSummary
from models.position_wise_ff import PositionwiseFF


class Transformers_joint_slot_and_intent(nn.Module):

    def __init__(self, pretrained_model_type, pretrained_model, tagset_size, class_size,
                 decoder_num_heads=3, decoder_num_layers=3, dropout=0., device=None,
                 extFeats_dim=None, multi_class=False, task_st='NN', task_sc='CLS'):
        """Initialize model."""
        super(Transformers_joint_slot_and_intent, self).__init__()
        self.intent_embedding_dim = 128
        self.slot_embedding_dim = 128
        self.slot_attend_output_dim = 128
        self.decoder_num_heads = decoder_num_heads
        self.decoder_num_layers = decoder_num_layers
        self.tagset_size = tagset_size
        self.class_size = class_size
        self.dropout = dropout
        self.device = device
        self.extFeats_dim = extFeats_dim
        self.multi_class = multi_class
        self.task_st = task_st  # 'NN', 'NN_crf'
        self.task_sc = task_sc  # None, 'CLS', 'max', 'CLS_max'

        self.dropout_layer = nn.Dropout(p=self.dropout)

        self.pretrained_model_type = pretrained_model_type
        self.pretrained_model = pretrained_model
        if self.pretrained_model_type == 'xlnet':
            self.sequence_summary = SequenceSummary(self.pretrained_model.config)
        self.embedding_dim = self.pretrained_model.config.hidden_size

        # The LSTM takes word embeddings as inputs, and outputs hidden states
        self.append_feature_dim = 0
        if self.extFeats_dim:
            self.append_feature_dim += self.extFeats_dim
            self.extFeats_linear = nn.Linear(self.append_feature_dim, self.append_feature_dim)
        else:
            self.extFeats_linear = None

        # The linear layer that maps from hidden state space to tag space
        if self.task_st == 'NN':
            self.hidden2tag = nn.Linear(self.embedding_dim + self.intent_embedding_dim + self.slot_attend_output_dim,
                                        # self.embedding_dim + self.append_feature_dim + self.intent_embedding_dim + self.slot_attend_output_dim,
                                        self.tagset_size)
        else:
            self.hidden2tag = nn.Linear(self.embedding_dim + self.append_feature_dim, self.tagset_size + 2)
            self.crf_layer = crf.CRF(self.tagset_size, self.device)
        if self.task_sc == 'CLS' or self.task_sc == 'max':
            self.hidden2class = nn.Linear(self.embedding_dim, self.class_size)
        elif self.task_sc == 'CLS_max':
            self.hidden2class = nn.Linear(self.embedding_dim * 2, self.class_size)
        else:
            pass

        self.intent2vec = nn.Embedding(self.class_size, self.intent_embedding_dim)
        self.slot2vec = nn.Embedding(self.tagset_size, self.slot_embedding_dim)
        self.attend_layers = nn.ModuleList()
        self.position_wise_ffs = nn.ModuleList()
        for _ in range(decoder_num_layers):
            self.position_wise_ffs.append(PositionwiseFF(self.slot_embedding_dim, self.slot_embedding_dim, self.dropout))
            self.attend_layers.append(SelfAttend(query_input_size=self.slot_embedding_dim,
                                                 key_input_size=self.slot_embedding_dim,
                                                 output_size=self.slot_attend_output_dim,
                                                 relative_pos=True,
                                                 num_heads=self.decoder_num_heads, masked=True, device=self.device))
        # self.init_weights()

    def init_weights(self, initrange=0.2):
        """Initialize weights."""
        if self.extFeats_linear:
            self.extFeats_linear.weight.data.uniform_(-initrange, initrange)
            self.extFeats_linear.bias.data.uniform_(-initrange, initrange)
        self.hidden2tag.weight.data.uniform_(-initrange, initrange)
        self.hidden2tag.bias.data.uniform_(-initrange, initrange)
        if self.task_sc:
            self.hidden2class.weight.data.uniform_(-initrange, initrange)
            self.hidden2class.bias.data.uniform_(-initrange, initrange)

    def forward(self, sentences, lengths, extFeats=None, masked_output=None, is_training=True):
        # step 1: word embedding
        tokens, segments, selects, copies, attention_mask = sentences['tokens'], sentences['segments'], \
                                                            sentences['selects'], sentences['copies'], \
                                                            sentences['mask']

        outputs = self.pretrained_model(tokens, token_type_ids=segments, attention_mask=attention_mask)
        if self.pretrained_model_type == 'bert':
            transformer_top_hiddens, transformer_cls_hidden = outputs[0:2]
        else:
            transformer_top_hiddens = outputs[0]
            transformer_cls_hidden = self.sequence_summary(transformer_top_hiddens)

        transformer_cls_hidden = transformer_top_hiddens[:, 0]
        batch_size, transformer_seq_length, hidden_size = transformer_top_hiddens.size(0), \
                                                          transformer_top_hiddens.size(1), \
                                                          transformer_top_hiddens.size(2)
        chosen_encoder_hiddens = transformer_top_hiddens.view(-1, hidden_size).index_select(0, selects)
        max_len = max(lengths)
        embeds = torch.zeros(len(lengths) * max_len, hidden_size, device=self.device)
        embeds = embeds.index_copy_(0, copies, chosen_encoder_hiddens).view(len(lengths), max_len, -1)
        if type(extFeats) != type(None):
            concat_input = torch.cat((embeds, self.extFeats_linear(extFeats)), 2)
        else:
            concat_input = embeds

        # step 2: intent classifier
        if self.task_sc:
            if self.task_sc == 'CLS':
                hidden_for_intent = transformer_cls_hidden
            elif self.task_sc == 'max':
                hidden_for_intent = embeds.max(1)[0]
            else:
                hidden_for_intent = torch.cat((transformer_cls_hidden, embeds.max(1)[0]), dim=1)
            class_space = self.hidden2class(self.dropout_layer(transformer_cls_hidden))
            if self.multi_class:
                class_scores = torch.sigmoid(class_space)
                if type(masked_output) != type(None):
                    class_scores.index_fill_(1, masked_output, 0)
            else:
                class_scores = F.log_softmax(class_space, dim=1)
        else:
            class_scores = None

        """
        # step 3: slot tagger
        if is_training:
            class_idxs, tag_idxs = sentences['class_idxs'], sentences['shift_tags_idxs']
            repeated_intent_embeddings = self.intent2vec(class_idxs).view(batch_size, 1, -1).repeat(1, max_len, 1)
            tag_embeddings = self.slot2vec(tag_idxs)
            tag_embeddings = self.attend_layer(tag_embeddings, tag_embeddings)
            concat_input = torch.cat([concat_input, repeated_intent_embeddings, tag_embeddings], dim=2)
            concat_input_reshape = concat_input.contiguous().view(concat_input.size(0) * concat_input.size(1),
                                                                  concat_input.size(2))
            tag_space = self.hidden2tag(self.dropout_layer(concat_input_reshape))
        else:
            class_idxs = torch.argmax(class_scores, dim=1)
            tag_idxs = torch.zeros([batch_size], device=self.device, dtype=torch.long)
            intent_embedding = self.intent2vec(class_idxs)
            repeated_intent_embeddings = intent_embedding.view(batch_size, 1, -1).repeat(1, max_len, 1)
            concat_input = torch.cat([concat_input, repeated_intent_embeddings], dim=2)
            tag_space = []
            slot_outputs = torch.zeros([batch_size, max_len, self.slot_embedding_dim], device=self.device)
            for i in range(max_len):
                tag_embeddings = self.slot2vec(tag_idxs)
                slot_outputs[:, i] = tag_embeddings

                attend_vector = self.attend_layer(tag_embeddings.view(batch_size, 1, -1), slot_outputs, mask_pos=i)
                tag_space_i = self.hidden2tag(torch.cat([concat_input[:, i, :], attend_vector.view(batch_size, -1)], dim=1))
                #tag_space_i = self.hidden2tag(torch.cat([concat_input[:, i, :], tag_embeddings], dim=1))
                tag_idxs = torch.argmax(tag_space_i, dim=1)
                tag_space.append(tag_space_i)
            tag_space = torch.stack(tag_space, dim=1).view(batch_size * max_len, -1)
        """

        if is_training:
            class_idxs, tag_idxs = sentences['class_idxs'], sentences['shift_tags_idxs']
            intent_embeddings = self.intent2vec(class_idxs).view(batch_size, 1, -1).repeat([1, concat_input.size(1), 1])
            tag_embeddings = self.slot2vec(tag_idxs)

            self.attend_layers[0](tag_embeddings, tag_embeddings)

            attend_input = tag_embeddings
            for i in range(self.decoder_num_layers):
                attend_output = self.position_wise_ffs[i](self.attend_layers[i](attend_input, attend_input))
                attend_input = attend_output

            concat_input = torch.cat([concat_input, intent_embeddings, attend_output], dim=-1)
            #concat_input = torch.cat([concat_input, attend_vector], dim=-1)
            #concat_input_reshape = concat_input.view(concat_input.size(0) * concat_input.size(1), -1)
            concat_input_reshape = concat_input.view(concat_input.size(0) * concat_input.size(1), -1)
            tag_space = self.hidden2tag(self.dropout_layer(concat_input_reshape))
        else:
            class_idxs = torch.argmax(class_scores, dim=1)
            tag_idxs = torch.zeros([batch_size], device=self.device, dtype=torch.long)
            intent_embedding = self.intent2vec(class_idxs)
            tag_space = []

            attend_results = torch.zeros([batch_size, len(self.attend_layers), max_len, self.slot_embedding_dim],
                                         device=self.device)
            for i in range(max_len):
                tag_embeddings = self.slot2vec(tag_idxs)
                attend_results[:, 0, i, :] = tag_embeddings
                for j in range(self.decoder_num_layers):
                    attend_vector = self.attend_layers[j](attend_results[:, j, i, :].view(batch_size, 1, -1),
                                                          attend_results[:, j, :, :], mask_pos=i).view(batch_size, -1)
                    attend_vector = self.position_wise_ffs[j](attend_vector)
                    if j + 1 < self.decoder_num_layers:
                        attend_results[:, j + 1, i, :] = attend_vector

                slot_vector = torch.cat([concat_input[:, i], intent_embedding, attend_vector], dim=-1)
                tag_space_i = self.hidden2tag(slot_vector)
                tag_idxs = torch.argmax(tag_space_i, dim=1)
                tag_space.append(tag_space_i)
            tag_space = torch.stack(tag_space, dim=1).view(batch_size * max_len, -1)

        if self.task_st == 'NN':
            tag_scores = F.log_softmax(tag_space, dim=1)
        else:
            tag_scores = tag_space
        tag_scores = tag_scores.view(concat_input.size(0), concat_input.size(1), tag_space.size(1))

        return tag_scores, class_scores

    def load_model(self, load_dir):
        if self.device.type == 'cuda':
            self.load_state_dict(torch.load(open(load_dir, 'rb')))
        else:
            self.load_state_dict(torch.load(open(load_dir, 'rb'), map_location=lambda storage, loc: storage))

    def save_model(self, save_dir):
        torch.save(self.state_dict(), open(save_dir, 'wb'))