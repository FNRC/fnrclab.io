import torch
import torch.nn as nn
import torch.nn.functional as F


class PositionalEmbedding(nn.Module):
    def __init__(self, d):
        super().__init__()
        self.d = d
        inv_freq = 1 / (10000 ** (torch.arange(0.0, d, 2.0) / d))
        self.register_buffer("inv_freq", inv_freq)

    def forward(self, pos_seq,  # (seq, )
                ):
        # outer product
        sinusoid_inp = torch.ger(pos_seq, self.inv_freq)
        #sinusoid_inp = torch.einsum("i,j->ij", pos_seq, self.inv_freq)
        pos_emb = torch.cat([sinusoid_inp.sin(), sinusoid_inp.cos()], dim=-1)
        return pos_emb[None, :, :]


class PositionwiseFF(nn.Module):
    def __init__(self, d_model, d_inner, dropout, pre_lnorm=False):
        super(PositionwiseFF, self).__init__()

        self.d_model = d_model
        self.d_inner = d_inner
        self.dropout = dropout

        self.CoreNet = nn.Sequential(
            nn.Linear(d_model, d_inner), nn.ReLU(inplace=True),
            nn.Dropout(dropout),
            nn.Linear(d_inner, d_model),
            nn.Dropout(dropout),
        )

        self.layer_norm = nn.LayerNorm(d_model)

        self.pre_lnorm = pre_lnorm

    def forward(self, inp):
        if self.pre_lnorm:
            ##### layer normalization + positionwise feed-forward
            core_out = self.CoreNet(self.layer_norm(inp))

            ##### residual connection
            output = core_out + inp
        else:
            ##### positionwise feed-forward
            core_out = self.CoreNet(inp)

            ##### residual connection + layer normalization
            output = self.layer_norm(inp + core_out)

        return output


class SelfAttend(nn.Module):
    def __init__(self, query_input_size, key_input_size, output_size, num_heads, is_training=False, pos_emb_dim=32,
                 dropout_rate=0.0, masked=False, device=None, relative_pos=False):
        super(SelfAttend, self).__init__()
        self.query_input_size = query_input_size
        self.key_input_size = key_input_size
        self.output_size = output_size
        self.num_heads = num_heads
        self.masked = masked
        self.relative_pos = relative_pos
        self.device = device
        self.hidden_size = output_size * num_heads
        self.queries_layer = nn.Linear(query_input_size, self.hidden_size)
        self.keys_layer = nn.Linear(key_input_size, self.hidden_size)
        self.values_layer = nn.Linear(key_input_size, self.hidden_size)
        self.output_layer = nn.Linear(self.hidden_size, self.output_size)
        #self.layer_norm = nn.LayerNorm(self.output_size)

        if relative_pos:
            self.start_pos_embedding = nn.Parameter(torch.rand(1, 1, pos_emb_dim))
            self.positionalEmbedding = PositionalEmbedding(pos_emb_dim)
            self.positions_layer = nn.Linear(pos_emb_dim, self.hidden_size)
            self.position_bias = nn.Parameter(torch.zeros(self.num_heads, self.output_size))
            self.content_bias = nn.Parameter(torch.zeros(self.num_heads, 1, self.output_size))

    def _rel_shift(self, x):
        zero_pad = torch.zeros((x.size(0), 1, *x.size()[2:]), device=x.device, dtype=x.dtype)
        return (torch.cat([zero_pad, x], dim=1)
                .view(x.size(1) + 1, x.size(0), *x.size()[2:])[1:]
                .view_as(x))

    def forward(self, x, y, mask_pos=-1):

        queries = self.queries_layer(x)
        keys = self.keys_layer(y)
        values = self.values_layer(y)

        queries = split_heads(queries, self.num_heads, self.output_size)
        keys = split_heads(keys, self.num_heads, self.output_size)
        values = split_heads(values, self.num_heads, self.output_size)

        queries /= self.query_input_size ** 0.5

        if self.relative_pos:
            seq_len = y.size(1)
            pos_idxs = torch.arange(seq_len - 2, -1, -1, dtype=torch.long, device=self.device)
            relative_pos_embeddings = self.positionalEmbedding(pos_idxs)
            relative_pos_embeddings = torch.cat([self.start_pos_embedding, relative_pos_embeddings], dim=1)
            pos_embeddings = self.positions_layer(relative_pos_embeddings)
            pos_embeddings = split_heads(pos_embeddings, self.num_heads, self.output_size)

            q_k_logits = torch.matmul(queries + self.content_bias, torch.transpose(keys, 2, 3))

            pos_attn = torch.einsum("ibhd,jhd->ijbh", queries.permute([2, 0, 1, 3]) + self.position_bias,
                                    pos_embeddings.view(self.num_heads, seq_len, -1).permute([1, 0, 2]))  # scale,
            pos_attn = self._rel_shift(pos_attn).permute([2, 3, 0, 1])

            logits = q_k_logits + pos_attn
        else:
            q_k_logits = torch.matmul(queries, torch.transpose(keys, 2, 3))
            logits = q_k_logits

        if self.masked:
            if mask_pos > -1:
                mask = torch.zeros([queries.size(2), keys.size(2)], device=self.device)
                mask[:, mask_pos+1:] = 1
            else:
                mask = 1 - torch.tril(torch.ones([queries.size(2), keys.size(2)], device=self.device))

            logits = logits.masked_fill(mask.byte(), -float('inf'))
            #q_k_exp = torch.exp(logits) * mask
            #weights = q_k_exp / torch.sum(q_k_exp, dim=-1, keepdim=True)

        weights = F.softmax(logits, dim=-1)
        output = torch.matmul(weights, values)

        reshaped_output = output.permute(0, 2, 1, 3).contiguous().view(output.size(0), output.size(2), self.hidden_size)
        return F.relu(self.output_layer(reshaped_output))


def split_heads(vec, num_heads, depth):
    seq_len = vec.size(1)
    return vec.view(-1, seq_len, num_heads, depth).permute(0, 2, 1, 3)
