#!/usr/bin/env python3

'''
@Time   : 2019-07-18 22:06:40
@Author : su.zhu
@Desc   :
'''

import torch
import itertools

def prepare_inputs_for_bert_xlnet(sentences, tags, tag2idx, word_lengths, tokenizer, cls_token_at_end=False, pad_on_left=False, cls_token='[CLS]', sep_token='[SEP]', pad_token=0, sequence_a_segment_id=0, cls_token_segment_id=1, pad_token_segment_id=0, device=None):
    """ Loads a data file into a list of `InputBatch`s
        `cls_token_at_end` define the location of the CLS token:
            - False (Default, BERT/XLM pattern): [CLS] + A + [SEP] + B + [SEP]
            - True (XLNet/GPT pattern): A + [SEP] + B + [SEP] + [CLS]
        `cls_token_segment_id` define the segment id associated to the CLS token (0 for BERT, 2 for XLNet)
    """
    """ output: {
        'tokens': tokens_tensor,        # input_ids
        'segments': segments_tensor,    # token_type_ids
        'mask': input_mask,             # attention_mask
        'indicator': selects_tensor,      # original_word_to_token_position
        'copies': copies_tensor         # original_word_position
        }
    """
    ## sentences are sorted by sentence length
    max_length_of_sentences = max(word_lengths)
    tokens = []
    segment_ids = []
    indicator_ids = []
    new_tags = []

    for word_list, tag_list in zip(sentences, tags):
        new_tag = []
        indicator_id = []
        ts = []
        for i, word in enumerate(word_list):
            pieces = tokenizer.tokenize(word)
            if len(pieces) == 0:
                continue

            new_tag.append(tag_list[i])
            indicator_id.append(1)
            for _ in range(1, len(pieces)):
                new_tag.append(tag2idx['<pad>'])
                indicator_id.append(0)

            ts += pieces
        ts += [sep_token]
        si = [sequence_a_segment_id] * len(ts)
        if cls_token_at_end:
            ts = ts + [cls_token]
            si = si + [cls_token_segment_id]
        else:
            ts = [cls_token] + ts
            si = [cls_token_segment_id] + si
        tokens.append(ts)
        segment_ids.append(si)
        new_tags.append(new_tag)
        indicator_ids.append(indicator_id)

    max_length_of_tokens = max([len(tokenized_text) for tokenized_text in tokens])
    #if not cls_token_at_end: # bert
    #    assert max_length_of_tokens <= model_bert.config.max_position_embeddings
    padding_lengths = [max_length_of_tokens - len(tokenized_text) for tokenized_text in tokens]
    if pad_on_left:
        input_mask = [[0] * padding_lengths[idx] + [1] * len(tokenized_text) for idx,tokenized_text in enumerate(tokens)]
        indexed_tokens = [[pad_token] * padding_lengths[idx] + tokenizer.convert_tokens_to_ids(tokenized_text) for
                          idx, tokenized_text in enumerate(tokens)]
        segments_ids = [[pad_token_segment_id] * padding_lengths[idx] + si for idx,si in enumerate(segment_ids)]
    else:
        input_mask = [[1] * len(tokenized_text) + [0] * padding_lengths[idx] for idx,tokenized_text in enumerate(tokens)]
        indexed_tokens = [tokenizer.convert_tokens_to_ids(tokenized_text) + [pad_token] * padding_lengths[idx] for
                          idx, tokenized_text in enumerate(tokens)]
        segments_ids = [si + [pad_token_segment_id] * padding_lengths[idx] for idx,si in enumerate(segment_ids)]
        new_tags = [new_tag + [tag2idx['<pad>']] * padding_lengths[idx] for idx,new_tag in enumerate(new_tags)]
        indicator_ids = [indicator_id + [0] * padding_lengths[idx] for idx,indicator_id in enumerate(indicator_ids)]

    input_mask = torch.tensor(input_mask, dtype=torch.long, device=device)
    tokens_tensor = torch.tensor(indexed_tokens, dtype=torch.long, device=device)
    segments_tensor = torch.tensor(segments_ids, dtype=torch.long, device=device)
    tags_tensor = torch.tensor(new_tags, dtype=torch.long, device=device)
    indicator_tensor = torch.tensor(indicator_ids, dtype=torch.long, device=device)
    return {'tokens': tokens_tensor, 'segments': segments_tensor, 'tags': tags_tensor, 'indicators': indicator_tensor, 'mask': input_mask, 'seq_length': max_length_of_tokens - 2}
