import tensorflow as tf
from DataParser import DataParser


def init_weights(params):
    embedding_size = params['embedding_size']

    weights = dict()
    weights_initializer = tf.glorot_normal_initializer(dtype=tf.float32)
    bias_initializer = tf.constant_initializer(dtype=tf.float32)

    names = ['feature', 'word', 'item', 'author', 'music', 'item_city', 'video', 'audio', 'item_uid', 'author_uid', 'music_uid']
    for name in names:
        key = '%s_embeddings' % name
        vocabulary_size = params['%s_size' % name]
        weights[key] = tf.get_variable(
            name=key,
            dtype=tf.float32,
            initializer=weights_initializer,
            shape=[vocabulary_size, embedding_size])

        first_order_key = 'first_order_%s_weights' % name
        weights[first_order_key] = tf.get_variable(
            name=first_order_key,
            dtype=tf.float32,
            initializer=weights_initializer,
            shape=[vocabulary_size, 1])

    weights['fm_bias'] = tf.get_variable(name='fm_bias', dtype=tf.float32, initializer=bias_initializer, shape=[1])

    return weights


def split_heads(x, num_heads, depth):

    input_length = tf.shape(x)[1]
    x = tf.reshape(x, [-1, input_length, num_heads, depth])
    return tf.transpose(x, [0, 2, 1, 3])


def combine_heads(x, num_heads, depth):

    input_length = tf.shape(x)[2]
    x = tf.transpose(x, [0, 2, 1, 3])

    return tf.reshape(x, [-1, input_length, num_heads * depth])


def attend_layer(x, y, num_heads, embedding_size, is_training=False, dropout_rate=0.0):
    hidden_size = embedding_size * num_heads

    queries = tf.layers.dense(inputs=x, units=hidden_size, use_bias=False,
                              name="query")

    keys = tf.layers.dense(inputs=y, units=hidden_size, use_bias=False,
                           name="key")

    values = tf.layers.dense(inputs=y, units=hidden_size, use_bias=False,
                             name="value")

    # split q, k, v into heads
    queries = split_heads(queries, num_heads, embedding_size)
    keys = split_heads(keys, num_heads, embedding_size)
    values = split_heads(values, num_heads, embedding_size)

    queries /= embedding_size ** -0.5

    logits = tf.matmul(queries, keys, transpose_b=True)
    # bias = tf
    # logits += bias
    weights = tf.nn.softmax(logits)
    if is_training:
        weights = tf.layers.dropout(weights, dropout_rate)
    attention_output = tf.matmul(weights, values)

    attention_output = combine_heads(attention_output, num_heads, embedding_size)

    attention_output = tf.layers.dense(attention_output, units=embedding_size, use_bias=False,
                                       name="output_transform")
    return attention_output


def self_attend(embeddings, params, is_training):
    embedding_size = params['embedding_size']
    num_heads = params['attention_num_heads']
    num_layers = params['attention_num_layers']
    dropout_rate = params['attention_dropout']

    attention_output = embeddings
    for l in range(num_layers):
        with tf.variable_scope("attention_%d" % l):
            attention_output = attend_layer(attention_output, attention_output, num_heads, embedding_size, is_training=is_training, dropout_rate=dropout_rate)

    return attention_output


def attend(x, y, params):

    embedding_size = params['embedding_size']
    num_heads = params['attention_num_heads']

    return attend_layer(x, y, num_heads, embedding_size, is_training=False)


def embed(features, weights, params):
    """ embed raw feature to embedding vector"""
    first_order_output_list = []
    embedding_list = []

    names = ['feature', 'word', 'item', 'author', 'music', 'item_city', 'item_uid', 'author_uid', 'music_uid']
    for name in names:
        field_size = params['%s_field_size' % name]
        feature_ids = tf.reshape(features['%s_ids' % name], shape=[-1, field_size])  # 特征id
        feature_values = tf.reshape(features['%s_weights' % name], shape=[-1, field_size, 1])  # 特征值

        first_order_weights = tf.nn.embedding_lookup(weights['first_order_%s_weights' % name], ids=feature_ids)
        embeddings = tf.nn.embedding_lookup(weights['%s_embeddings' % name], ids=feature_ids)

        if name == 'feature':
            first_order_output = tf.reduce_sum(tf.multiply(feature_values, first_order_weights), axis=2)
            embeddings = tf.multiply(feature_values, embeddings)
        else:
            first_order_output = tf.reduce_sum(tf.multiply(feature_values, first_order_weights), axis=1)
            embeddings = tf.reduce_sum(tf.multiply(feature_values, embeddings), axis=1, keepdims=True)

        first_order_output_list.append(first_order_output)
        embedding_list.append(embeddings)

    for name in ['video', 'audio']:
        feature_values = features['%s_weights' % name]
        first_order_output = tf.matmul(feature_values, weights['first_order_%s_weights' % name])
        embeddings = tf.reshape(tf.matmul(feature_values, weights['%s_embeddings' % name]), shape=[-1, 1, params['embedding_size']])
        first_order_output_list.append(first_order_output)
        embedding_list.append(embeddings)

    first_order_output = tf.concat(first_order_output_list, axis=1)
    embeddings = tf.concat(embedding_list, axis=1)

    return first_order_output, embeddings


class Attention:

    def __init__(self, use_dnn=True, use_cin=True, use_fm=False):
        self.use_dnn = use_dnn
        self.use_cin = use_cin
        self.use_fm = use_fm

    @staticmethod
    def model_fn(features, labels, mode, params):
        # parse params
        embedding_size = params['embedding_size']  # 字段嵌入大小
        learning_rate = params['learning_rate']  # 学习率
        field_size = params['feature_field_size'] + 10  # 字段数量
        hidden_units = params['hidden_units']  # 各隐藏层隐藏单元数
        dropout_rate = params.get('dropout_rate', 0.5)
        use_dnn = params.get('use_dnn', True)  # 是否使用Deep Neural Network(DNN), 默认True
        use_attention = params.get('use_attention', True)  # 是否使用Compressed Interactive Network(CIN), 默认True

        training = False
        if mode == tf.estimator.ModeKeys.TRAIN:
            training = True

        tf.logging.info(params)
        tf.logging.info('is_training' + str(training))

        weights = init_weights(params)  # 初始化权重
        first_order_outputs, embeddings = embed(features, weights, params)  # 特征嵌入
        last_layer_to_concat = [first_order_outputs]

        attended_outputs = self_attend(embeddings, params, is_training=training)  # 应用attention到特征
        attended_outputs = tf.reshape(attended_outputs, shape=[-1, field_size * embedding_size])
        attended_outputs = tf.layers.dense(attended_outputs, units=64, activation=tf.nn.relu)
        attended_outputs = tf.layers.dropout(attended_outputs, rate=dropout_rate, training=training)
        last_layer_to_concat.append(attended_outputs)

        # DNN part
        if use_dnn:
            nn_outputs = tf.reshape(embeddings, shape=[-1, field_size * embedding_size])

            for units in hidden_units:
                nn_outputs = tf.layers.dense(nn_outputs, units, activation=tf.nn.relu)
                nn_outputs = tf.layers.dropout(nn_outputs, rate=dropout_rate, training=training)

            last_layer_to_concat.append(nn_outputs)

        # Output layer
        outputs = tf.concat(last_layer_to_concat, axis=1)
        outputs = tf.layers.dense(outputs, 25, activation=tf.nn.relu)
        outputs = tf.layers.dropout(outputs, rate=dropout_rate, training=training)
        logits = tf.layers.dense(outputs, 1, activation=None)

        if mode == tf.estimator.ModeKeys.TRAIN:
            labels = tf.reshape(labels, shape=[-1, 1])  # 样本标签
            loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=logits, labels=labels))
            optimizer = tf.train.AdagradOptimizer(learning_rate=learning_rate, initial_accumulator_value=1e-8)
            train_op = optimizer.minimize(loss, global_step=tf.train.get_global_step())
            return tf.estimator.EstimatorSpec(
                mode=mode,
                loss=loss,
                train_op=train_op
            )

        if mode == tf.estimator.ModeKeys.PREDICT:
            predictions = {
                'probabilities': tf.sigmoid(logits),
                'logits': logits,
            }
            return tf.estimator.EstimatorSpec(
                mode=mode,
                predictions=predictions)

        if mode == tf.estimator.ModeKeys.EVAL:
            labels = tf.reshape(labels, shape=[-1, 1])  # 样本标签
            probabilities = tf.sigmoid(logits)
            predictions = tf.cast(probabilities >= 0.5, dtype=tf.float32)
            eval_metric_ops = {"auc": tf.metrics.auc(labels, probabilities),
                               "accuracy": tf.metrics.accuracy(labels=labels, predictions=predictions)}
            loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=logits, labels=labels))
            return tf.estimator.EstimatorSpec(
                mode=mode,
                loss=loss,
                eval_metric_ops=eval_metric_ops)


if __name__ == '__main__':
    from DataParser import DataParser

    dataParser = DataParser(label_name='finish', popped_name='like')
    features = {'feature_ids': tf.placeholder(dtype=tf.int32, shape=[32, 23]),
                'feature_weights': tf.placeholder(dtype=tf.float64, shape=[32, 23]),
                'video_weights': tf.placeholder(dtype=tf.float64, shape=[32, 128]),
                'audio_weights': tf.placeholder(dtype=tf.float64, shape=[32, 128]),
                'word_ids': tf.placeholder(dtype=tf.int32, shape=[32, 25]),
                'word_weights': tf.placeholder(dtype=tf.float64, shape=[32, 25]),
                'item_ids': tf.placeholder(dtype=tf.int32, shape=[32, 300]),
                'item_weights': tf.placeholder(dtype=tf.float64, shape=[32, 300]),
                'author_ids': tf.placeholder(dtype=tf.int32, shape=[32, 300]),
                'author_weights': tf.placeholder(dtype=tf.float64, shape=[32, 300]),
                'music_ids': tf.placeholder(dtype=tf.int32, shape=[32, 300]),
                'music_weights': tf.placeholder(dtype=tf.float64, shape=[32, 300]),
                'item_uid_ids': tf.placeholder(dtype=tf.int32, shape=[32, 150]),
                'item_uid_weights': tf.placeholder(dtype=tf.float64, shape=[32, 150]),
                'author_uid_ids': tf.placeholder(dtype=tf.int32, shape=[32, 500]),
                'author_uid_weights': tf.placeholder(dtype=tf.float64, shape=[32, 500]),
                'music_uid_ids': tf.placeholder(dtype=tf.int32, shape=[32, 500]),
                'music_uid_weights': tf.placeholder(dtype=tf.float64, shape=[32, 500]),
                }

    labels = tf.placeholder(dtype=tf.float64, shape=[32, 1])
    params = {
        'embedding_size': 40,
        'batch_size': 32,
        'feature_field_size': dataParser.field_size,
        'feature_size': dataParser.feature_size,
        'hidden_units': [200, 100, 75, 50, 25],
        'cin_layer_size': [50, 50, 50, 50],
        'word_size': 134600,
        'word_field_size': 25,
        'item_size': 4122689,
        'item_field_size': 300,
        'author_size': 850308,
        'author_field_size': 300,
        'music_size': 89779,
        'music_field_size': 300,
        'video_size': 128,
        'audio_size': 128,
        'video_field_size': 128,
        'audio_field_size': 128,
        'item_uid_size': 73974,
        'item_uid_field_size': 150,
        'author_uid_size': 73974,
        'author_uid_field_size': 500,
        'music_uid_size': 73974,
        'music_uid_field_size': 500,
        'learning_rate': 0.005,
        'attention_num_heads': 8,
        'attention_num_layers': 6,
        'attention_dropout': 0.2
    }

    estimatorSpec = Attention.model_fn(features, labels, mode=tf.estimator.ModeKeys.TRAIN, params=params)
    print(estimatorSpec)
