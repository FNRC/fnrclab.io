Vue.component('app-nav', {
  props: [],
  template: '<div class="navbar">'+
      '<a href="http://www.seu.edu.cn" target="_blank">东南大学</a>'+
      '<a href="http://cse.seu.edu.cn" target="_blank">计算机学院</a>' + 
      '<a href="https://gitlab.com/FNRC" target="_blank">FNRC GitLab</a>' + 
    '</div>'
})

Vue.component('app-info-item', {
  props: ['info'],
  template: '<tr><td> {{ info.title }} </td><td> {{ info.ip }} </td></tr>'
})

new Vue({
        el: '#app',
        data: {
                
        },
        created () {
          fetch('https://fnrc.gitlab.io/address.json')
            .then(response => response.json())
            .then(json => {
                this.apps = json.apps
            })
        }
})

