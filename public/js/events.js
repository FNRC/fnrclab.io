function bubleSort(arr){
  var len = arr.length;
  for(var i=0; i < len - 1; i++){
    var max = arr[0];
    var max_index = 0;
    for (var j=1; j < len -i ; j++) {
      if (max[1]['pos'] < arr[j][1]['pos'] || (max[1]['pos']==arr[j][1]['pos'] && max[1]['tf-idf'] >= arr[j][1]['tf-idf'])){
        max = arr[j];
        max_index = j;
      }
    }
    arr[max_index] = arr[len -i -1];
    arr[len -i -1] = max;
  }
  return arr;
}

function renderHotEventlines(hot_eventlines){
  var series = [];
  for(var i=0; i < hot_eventlines.relatedUCL.length; i++){
    var obj = {name: '', type: 'spline', showInLegend: true,　dataPoints:[]};
    var eventline = hot_eventlines.relatedUCL[i];
    var keywords = JSON.parse(eventline[2]);
    var keeped_keywords = [keywords[0]];
    var sum = 0;
    for (var j=1; j < keywords.length; j++){
      var tfIdf = keywords[j][1]['tf-idf'];
      var oldVal = keywords[j-1][1]['tf-idf'];
      if(j > 1 && tfIdf < 0.01){
        break;
      }
      sum += tfIdf;
      keeped_keywords.push(keywords[j]);
    }
    
    bubleSort(keeped_keywords);
    
    obj['name'] = keeped_keywords[0][0];
    var word_num = 1;
    for(var j=1; j< keeped_keywords.length; j++){
      var keyword=keeped_keywords[j];
      var before_keyword = keeped_keywords[j-1];
      diff_pos = keyword[1]['pos']-before_keyword[1]['pos'];
      diff_tfidf = before_keyword[1]['tf-idf'] - keyword[1]['tf-idf'];
      diff_ratio = diff_tfidf > 0 ? diff_tfidf / before_keyword[1]['tf-idf'] : -1 * diff_tfidf / keyword[1]['tf-idf'];
      
      if(keyword[0].length==1 || (diff_pos == 0 && diff_ratio < 0.2)){
        continue;
      }

      if((diff_pos == 0 && diff_ratio > 0.26) || diff_pos > 1)
        obj['name'] += ' ';
      obj['name'] += keyword[0];
      word_num+=1;
      if (word_num ==4)
        break;
    }

    var events = eventline.relatedUCL;
    for(var j=0; j < events.length; j++){
      var event = events[j];
      var event_info = JSON.parse(event[11]);
      obj['dataPoints'].push({x: new Date(event_info['time']), y: event_info['doc_num'], title: event[1]});
    }
    obj.toolTipContent='{y} : {title}';
    series.push(obj);
  }
	
  var chart = new CanvasJS.Chart("hot-events", {
    theme:"light2",
    animationEnabled: true,
    zoomEnabled: true,
    title:{
      text: "24小时TOP25事件趋势图-Beta_1.0 (来源: 新浪、网易、中新网)",
      fontSize: 18
    },
    toolTip:{
      shared: true,
    },
    axisY :{
      includeZero: false,
      title: "报道数",
    },
    legend:{
      cursor:"pointer",
      itemclick : toggleDataSeries
    },
    data: series,
  });
  chart.render();

  function toggleDataSeries(e) {
    if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible ){
      e.dataSeries.visible = false;
    } else {
      e.dataSeries.visible = true;
    }
    chart.render();
  }
}

if(window.WebSocket){
  var ws = new WebSocket('wss://events.publicvm.com:8001');
　　ws.binaryType='arraybuffer';
　　ws.onopen = function(e){
    console.log("连接服务器成功，加载数据中");
    ws.send('{"path":"hot-events", "limit": 25}');
　　}
　　ws.onclose = function(e){
　　　　console.log("服务器关闭");
　　}
　　ws.onerror = function(){
　　　　console.log("连接出错");
　　}

　　ws.onmessage = function(e){
　　　　console.log("收到UCL数据包，正在解析中...");				
　　　　var data = new UCLPropertySet();
　　　　data.unpack(new Uint8Array(e.data));
　　　　console.log("UCL数据包解析完毕，即将打印输出");
　　　　console.log("UCL对象: ", data);
　　　　if(data.getType()==0){
　　　　　　renderHotEventlines(data);
//      setTimeout(function(){console.log("获取最新数据中..."); ws.send('{"path":"hot-events", "limit":25}');}, 10000);
　　　　}else if(data.getType()==1){
　　　　　　var event = data;
　　　　　　console.log(event);
　　　　}
　　}
}
